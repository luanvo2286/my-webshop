<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';
    protected $fillabe = ['name', 'image', 'price'];

    public function scopeNameSearch($query)
    {
        if(request()->key){
            
            $key = request()->key;
            $query->where('name', 'LIKE', '%'.$key.'%');
        }

        return $query;
    }
    public function scopePriceSearch($query)
    {
        if(request()->price){
            
            $key = explode('-',request()->price);
            $query->whereBetween('price', [(int)$key[0], (int)$key[1]]);
        }

        return $query;
    }
    public function scopePriceRangeSearch($query)
    {
        if(request()->price){
            
            $key = request()->price;
            $query->whereBetween('price', [(int)$key['start'], (int)$key['end']]);
        }

        return $query;
    }
    public function scopeCategorySearch($query)
    {
        if(request()->category){
            $key = request()->category;
            $query->join('categories', 'products.id_category', '=', 'categories.id')
                    ->where('categories.id', '=', $key);
        }
        
        return $query;
    }
    public function scopeBrandSearch($query)
    {
        if(request()->brand){
            $key = request()->brand;
            $query->join('brands', 'products.id_brand', '=', 'brands.id')
                ->where('brands.id', '=', $key);
        }

        return $query;
    }

    public function scopeStatusSearch($query){
        if(isset(request()->status)){
            $key = request()->status;
            $query->where('status', '=', $key);
        }
        return $query;
    }
}
