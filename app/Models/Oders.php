<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Oders extends Model
{
    use HasFactory;

    protected $table = 'Oders';

    protected $fillable = ['id_user', 'status', 'id_custommer'];
}
