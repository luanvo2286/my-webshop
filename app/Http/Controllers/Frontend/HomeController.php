<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Oder_items;
use App\Models\Oders;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //6 lastest product.
        $dataProducts = DB::table('products')
                        ->orderBy('updated_at', 'desc')
                        ->get()->toArray();
        
//------------------------------------------------------------------------
        //cart product
        if(!session()->has('cart')){
            $getOrderStatus = Oders::where('id_user', Auth::id())->where('status', 0)->get()->toArray();
            if(!empty($getOrderStatus)){
                $idOrder = $getOrderStatus[0]['id'];
                //  dd($idOrder);
        
                $dataOrderItems = Oder_items::where('id_oder', $idOrder)->get()->toArray();
                //  dd($dataOrderItems);
                foreach($dataOrderItems as $item){
                    $arr['id'] = $item['id_product'];
                    $arr['qty'] = $item['qty'];
        
                    session()->push('cart',$arr);
                }
            }
        }
        
//-------------------------------------------------------------------------
        foreach($dataProducts as $key=>$item){
            if($key < 6){
                $sixLastest[] = $item;
            } 
        }
        
        return view('frontend/home', ['sixLastest'=>$sixLastest]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
