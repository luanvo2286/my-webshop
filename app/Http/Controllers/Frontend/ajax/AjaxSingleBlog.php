<?php

namespace App\Http\Controllers\Frontend\ajax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Rating;

class AjaxSingleBlog extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function rateProduct(Request $request){

        $totalRate = 0;
        $avrgRate = 0;
        $qtyRate = 0;
        $response = array();

        $rateCheck = Rating::where([
                                'product_id' => $request->product_id,
                                'user_id' => $request->user_id])
                            ->get()->toArray();
                           

     
        if(!empty($rateCheck)){
            $response['info'] = 'user da rate!';
            return response()->json($response); 
        }else{
            $rate = new Rating();
            $rate->product_id = $request->product_id;
            $rate->user_id = $request->user_id;
            $rate->rate_star = $request->rate_value;
            
            if($rate->save()){
                $rateNew = Rating::where([
                    'product_id' => $request->product_id,
                    ])
                    ->get()->toArray();
                foreach($rateNew as $item){
                    $totalRate += $item['rate_star'];
                    $qtyRate += 1;
                }
                $avrgRate = $totalRate/($qtyRate);

                $response['success'] = 'Save OK!';
                $response['data'] = $avrgRate;

                return response()->json($response);
            }
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
