<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function searchName(){
        $listProductRaw = Product::namesearch()->paginate(6);
        $data = request()->toArray();
        //dd($data);
        return view('frontend/search/search', ['listProduct'=>$listProductRaw, 'dataSearch'=>$data]);
    }

    public function searchAdvanced(){
        //dd(request()->toArray());
        $brands = Brand::all()->toArray();
        $categories = Category::all()->toArray();

        $listProduct = Product::namesearch()
                                ->pricesearch()
                                ->categorysearch()
                                ->BrandSearch()
                                ->statussearch()
                                ->paginate(6);
        
        $data = request()->toArray();
        //dd($data);
        return view('frontend/search/searchadavnced', ['listProduct'=>$listProduct, 'brands'=>$brands, 'categories'=>$categories, 'currentData'=>$data]);
    }

    public function searchPost(){
        $brands = Brand::all()->toArray();
        $categories = Category::all()->toArray();

        $listProduct = Product::namesearch()
                                ->pricesearch()
                                ->categorysearch()
                                ->BrandSearch()
                                ->statussearch()
                                ->paginate(6);
        
        $data = request()->toArray();
        return view('frontend/search/searchadavnced', ['listProduct'=>$listProduct, 'brands'=>$brands, 'categories'=>$categories, 'currentData'=>$data]);
    }


    public function convert_vi_en($str){
       
            $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
            $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
            $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
            $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
            $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
            $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
            $str = preg_replace("/(đ)/", 'd', $str);
            $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
            $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
            $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
            $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
            $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
            $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
            $str = preg_replace("/(Đ)/", 'D', $str);
            //$str = str_replace(" “, “-”, str_replace(“&*#39;”,”",$str));
            return $str;
            
           
    }
}
