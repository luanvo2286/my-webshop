<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Requests\AddProductRequest;
use App\Http\Requests\EditProductRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Oders;
use Aws\Backup\BackupClient;
use Illuminate\Queue\Jobs\RedisJob;
use Illuminate\Support\Facades\Auth;
use Image;
use PhpParser\Node\Expr\Isset_;
use App\Models\Oder_items;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Product::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = new Product();
        if(is_null($product)){
            return response('Product Not Found!', 404);
        }
        //return....
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $product =[];
        $brand = [];
        $productArr = Product::where('id', $id)->get()->toArray();
        if(!empty($productArr)){
            $product = $productArr[0];
            $brand = Brand::where('id', $product['id_brand'])->get()->toArray()[0];
        }
        
        //dd($brand);
        return view('Frontend/product/product-detail', ['product'=>$product, 'brand'=>$brand]);
    }

    public function showProduct(){  
        
        $data = Product::where('id_user', Auth::id())->get()->toArray();
       
        return view('Frontend/product/product', ['dataProducts'=>$data]);
        
        
    }

    public function addProduct(){
        $categories = Category::all()->toArray();
        $brands = Brand::all()->toArray();
        
        return view('frontend/product/addnew', ['categories'=>$categories, 'brands'=>$brands]);
    }

    public function addProductPost(AddProductRequest $request){
        $product = new Product();
        //$data = $request->all();

        //dd($request->all());

        if($request->hasfile('image'))
        {
            foreach($request->file('image') as $key => $file)
            {
                $name = $file->getClientOriginalName();
                $name329 = "hinh329_".$file->getClientOriginalName();
                $name85 = "hinh85_".$file->getClientOriginalName();

                $path = public_path('uploads/frontend/product/' . $name);
                $path2 = public_path('uploads/frontend/product/' . $name329);
                $path3 = public_path('uploads/frontend/product/' . $name85);

                Image::make($file->getRealPath())->save($path);
                Image::make($file->getRealPath())->resize(84, 85)->save($path3);
                Image::make($file->getRealPath())->resize(200, 300)->save($path2);

                $data[] = $name;
            }
            
            $product->image = json_encode($data);
        }else{
            return redirect()->back()->withErrors('Image field NOT NULL!!..');
        }
        $product->name = $request['name'];
        $product->price = $request['price'];
        $product->id_user = Auth::id();
        $product->id_category = $request['category'];
        $product->id_brand = $request['brand'];
        $product->status = $request['sale'];
        if($request['sale']){
            $product->sale = $request['persent'];
        }else{
            $product->sale = 0;
        }
        
        $product->company = $request['company'];
        $product->detail = $request['detail'];

        if($product->save()){
            return redirect()->route('member.products.detail');
        }else{
            return redirect()->back()->withErrors('Add new Failed');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editProduct($id)
    {
        $dataProduct = Product::findOrFail($id)->toArray();

        //dd($dataProduct);
        $categories = Category::all()->toArray();
        $brands = Brand::all()->toArray();
        
        return view('frontend/product/edit', ['categories'=>$categories, 'brands'=>$brands, 'dataProduct'=>$dataProduct]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateProduct(EditProductRequest $request, $id)
    {
       $data = $request->toArray();

       //dd($data);
       $productWithID = Product::findOrFail($id);
       $productWithID->name = $data['name'];
       $productWithID->price = $data['price'];
       $productWithID->id_category = $data['category'];
       $productWithID->id_brand = $data['brand'];
       $productWithID->status = $data['sale'];
       if($data['sale']){
          $productWithID->sale = $data['persent'];
       }else{
        $productWithID->sale = 0;
       }
       $productWithID->company = $data['company'];
       $productWithID->detail = $data['detail'];


       $arrImg = json_decode($productWithID->image, true);

       $countArrImg = count($arrImg);
       $countCheck = 0;
       $countReq = 0;
       //dd($arrImg);
       //xoa image check o day
       if(isset($data['check'])){
           $countCheck = count($data['check']);
           //$numberImage = $countArrImg - $countCheck + $countReq;
            if($countArrImg == $countCheck){
                if(!$request->hasfile('image')){
                    return redirect()->back()->withErrors('Product Image field NOT NULL...');
                }else{
                    $countReq = count($request->file('image'));
                    $numberImage = $countArrImg - $countCheck + $countReq;
                    if($numberImage > 3){
                        return redirect()->back()->withErrors('Number of Image not over 3...');
                    }
                }
            }else{
                if(!$request->hasfile('image')){
                    $countReq = count($request->file('image'));
                    $numberImage = $countArrImg - $countCheck + $countReq;
                    if($numberImage > 3){
                        return redirect()->back()->withErrors('Number of Image not over 3...');
                    }
                }
                
            }

            foreach($data['check'] as $key=>$item){ 
                if(in_array($item, $arrImg)){
                    //dd($item);
                    if(file_exists('uploads/frontend/product/'.$item)){
                        unlink('uploads/frontend/product/'.$item);
                    }
                    if(file_exists('uploads/frontend/product/'.'hinh85_'.$item)){
                        unlink('uploads/frontend/product/'.'hinh85_'.$item);
                    }
                    if(file_exists('uploads/frontend/product/'.'hinh329_'.$item)){
                        unlink('uploads/frontend/product/'.'hinh329_'.$item);
                    }
                    unset($arrImg[$key]);
                    //dd($arrImg);
                }
            }
            
       }else{
            if($request->hasfile('image')){
                $countReq = count($request->file('image'));
                $numberImage = $countArrImg - $countCheck + $countReq;
                if($numberImage > 3){
                    return redirect()->back()->withErrors('Number of Image not over 3...');
                }
            }
            
       }

       $arr = array_values($arrImg);
    
       if($request->hasfile('image'))
        {
            foreach($request->file('image') as $key => $file)
            {
                $name = $file->getClientOriginalName();
                $name329 = "hinh329_".$file->getClientOriginalName();
                $name85 = "hinh85_".$file->getClientOriginalName();

                $path = public_path('uploads/frontend/product/' . $name);
                $path2 = public_path('uploads/frontend/product/' . $name329);
                $path3 = public_path('uploads/frontend/product/' . $name85);

                Image::make($file->getRealPath())->save($path);
                Image::make($file->getRealPath())->resize(50, 70)->save($path2);
                Image::make($file->getRealPath())->resize(200, 300)->save($path3);

                $arr[] = $name;
            }
            
        }
        $productWithID->image = json_encode($arr);
        if($productWithID->update()){
            return redirect()->route('member.products.detail');
        }else{
            return redirect()->back()->withErrors('Edit product failed!!');
        }
    }

    public function AddToCart(Request $req){ //gui len data json

        $productId = $req->id;
        
        //$productId = $req->toArray()['id_product'];
        $arr = [];
        $arr['id'] = $productId;
        $arr['qty'] = 1;

        //lấy ra trường có id_user hiên tại  và status 0
        $getOrderStatus = Oders::where('id_user', Auth::id())->where('status', 0)->get()->toArray();
        //check xem có status 0 của id_uer hiện tại ko (status 0 là những product đang nằm trong giỏ hàng)
        if(empty($getOrderStatus)){
            $order = new Oders();
            $order_item = new Oder_items();

            $order->id_user = Auth::id();
            $order->save();

            $getOrderStatus = Oders::where('id_user', Auth::id())->where('status', 0)->get()->toArray(); // lấy lại order

            $order_item->id_oder = $getOrderStatus[0]['id'];
            $order_item->id_product = $arr['id'];
            $order_item->qty = $arr['qty'];
            $order_item->save();
        }else{
            $getOrderItems = Oder_items::where('id_oder', $getOrderStatus[0]['id'])->get()->toArray();
            $flag = false;
            foreach($getOrderItems as $item){
                if($item['id_product'] == $productId){
                    $order_item = Oder_items::findOrFail($item['id']);
                    $order_item->qty = $item['qty'] + 1;
                    $order_item->update();
                    $flag = true;
                }
            }

            if(!$flag){
                $order_item = new Oder_items();
                $order_item->id_oder = $getOrderStatus[0]['id'];
                $order_item->id_product = $arr['id'];
                $order_item->qty = $arr['qty'];
                $order_item->save();
            }
        }
        //sau đó lưu thông tin product vào bảng order-items, bảng order-items, trong mỗi hàng thì có thể có ô id_order trùng nhau, 
        //vi 1 order có thể có nhiều product
        
        
        //đưa thong tin product vào session
        if(session()->has('cart')){
            $getSession = session()->get('cart');
            $flag = 1;
            $qtyAll = 1;
            foreach($getSession as $key=>$value){
                $qtyAll += $value['qty'];
                if($productId == $value['id']){
                    $getSession[$key]['qty'] += 1;
                    session()->put('cart', $getSession);
                    $flag = 0;
                }
            }

            if($flag == 1){
                session()->push('cart', $arr);
                
            }
            return response()->json(['qtyAll'=>$qtyAll], 200);
        }else{
            session()->push('cart', $arr);
            return response()->json(['qtyAll'=>1], 200);
        }


        //print_r(session()->get('cart'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
