<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\blog;
use App\Models\Rating;
use App\Models\Comments;
use Illuminate\Support\Facades\Auth;
use stdObject;

class BlogController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataBlogs = blog::paginate(3);
       // dd($dataBlogs);
        return view('frontend/blog/blog', compact('dataBlogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function showBlogSingle($id)
    {
        $totalRate = 0;
        $avrgRate = 0;
        $qtyRate = 0;
        $contentData = blog::findOrFail($id);
        //dd($contentData);
        
        $rate = Rating::where('product_id', $id)->get()->toArray(); 
        $cmts = Comments::where([
            'blog_id'=>$id
        ])->get()->toArray();

        //dd($cmts);

        if(!empty($rate)){
           
            foreach($rate as $item){
                
                $totalRate += $item['rate_star'];
                $qtyRate += 1;
            }
            $avrgRate = $totalRate/($qtyRate);
           
        }
       
        return view('frontend/blog/singleblog', ['contentData'=>$contentData, 'cmts'=>$cmts, 'product_id'=>$id, 'rateData' => $avrgRate]);
        
    }

    public function comments(Request $request, $id){
        
        $dataCmnt = ($request->toArray());

        $comment = new Comments();
        $comment->cmt = $dataCmnt['message'];
        $comment->blog_id = $id;
        $comment->user_id = Auth::id();
        $comment->name = Auth::user()->name;
        //dd(Auth::user()->avatar);
        if(is_null(Auth::user()->avatar)){
            //dd(Auth::user()->avatar);
            $comment->image = 'user-default.png';
        }else{
            $comment->image = Auth::user()->avatar;
        }
        $comment->level = $dataCmnt['level'];

        //dd($comment);
        if($comment->save()){
            return redirect()->back()->with('success','Comment added.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
