<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\registerMemberRequest;
use App\Http\Requests\loginMemberRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\conuntry;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('frontend/home');
    }

    public function register()
    {
        return view('frontend/member/register');
    }

    public function login()
    {
        //dd(session::all());
        return view('frontend/member/login');
        
    }

    public function logout()
    {

        Auth::logout();
        session()->flush();
        return redirect()->route('memberlogin');
    }

    public function storeMember(registerMemberRequest $request)
    {
        //chinh lai regisster
        $data = $request->all();
        //dd($data);
        $member = new User();
        $member->avatar = $this->UploadImg($request, 'avatar');
        $member->phone = $data['phone'];
        $member->id_country = $data['id_country'];
        $member->email = $data['email'];
        $member->name = $data['name'];
        $member->password = bcrypt($data['password']);
        $member->level = 0;
        if($member->save()){
            return redirect()->back()->with('success','Create Account success');
        }else{
            return redirect()->back()->withErrors('Create Account ERRR.....');
        }
    }

    public function loginMember(loginMemberRequest $request)
    {
        $dataLogin = [
            'email'=> $request->email,
            'password'=>$request->password,
            'level'=>0
        ];
        //dd($dataLogin);
        $remember = false;

        if($request->remember_me){
            $remember = true;
        }

        
        if(Auth::attempt($dataLogin, $remember)){
            return redirect()->route('home')->with('memberlogin', 'true');
        }else{
            return redirect()->back()->withErrors('Login Failed');
        }
    }

    public function showAccount(){

        $country = conuntry::all()->toArray();
        return view('Frontend/member/account', ['country'=>$country]);
        
    }

    public function updateAccount(UpdateProfileRequest $request){
        $data = $request->toArray();
        //dd($data);
        $member = User::findOrFail(Auth::id());
        
        if($fileAvatar = $this->UploadImg($request, 'avatar')){
            $member->avatar = $fileAvatar;
        }
        if($data['password']){
            $member->password = bcrypt($data['password']);
        }
        if($data['phone']){
            $member->phone = $data['phone'];
        }

        $member->name = $data['name'];
        $member->id_country = $data['id_country'];
        $member->address = $data['address'];
        if($member->update()){
            return redirect()->back()->with('success', 'update success!!'); 
        }else{
            return redirect()->back()->withErrors('update FAILD!!');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function UploadImg(Request $request, string $nameImg){
        $file = $request->file($nameImg);
        $fileName = '';
        
        if(empty($file)){
            return $fileName;
        }
        else{
            $fileName = $file->getClientOriginalName();
            $file->move(public_path('frontend/images/users'), $file->getClientOriginalName());
        }

        return $fileName;
    }
}
