<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Oder_items;
use App\Models\Oders;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $order = Oders::where('id_user', Auth::id())->where('status', 0)->get()->toArray();
        // if(!empty($order)){
        //     $getOrderItem = Oder_items::where('id_oder', $order[0]['id'])->get()->toArray();
        // }
        if(session()->has('cart')){
            $getCartProduct = session()->get('cart');
            foreach($getCartProduct as $item){
                $productInfo['product'] = Product::findOrFail($item['id'])->toArray();
                $productInfo['qty'] = $item['qty'];

                $productAll[] = $productInfo;
            }
            
        }else{
            $productAll = null;
        }
       // dd($productAll[0]['product']['image']);
        return view('frontend/cart/cart', ['productAll'=>$productAll]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
