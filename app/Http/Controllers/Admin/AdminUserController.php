<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateProfileRequest;
use App\Models\conuntry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AdminUserController extends Controller
{
    private $avatarType = ['png', 'jpg', 'jpeg'];
    public function __construct()
    {
        //$this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = conuntry::all();
        //dd(Auth::user()->id_country);
        return view('admin\user\pages-profile', ['countries'=>$countries]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfileRequest $request)
    {
       // dd($request);
        $userID = Auth::id();
        $user = User::findOrFail($userID);

        $data = $request->all();
        if($tempFile = $this->UploadImg($request)){
            $data['avatar'] = $tempFile;
        }else{
            $data['avatar'] = $user->avatar;
        }

        if($data['password']){
            $data['password'] = bcrypt($data['password']);
        }else{
            $data['password'] = $user->password;
        }
        if(!$data['phone']){
            $data['phone'] = $user->phone;
        }
        
        if($user->update($data)){
            return redirect()->back()->with('success',__('Update profile success'));
        }else{
            return redirect()->back()->withErrors('Update profile err');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function UploadImg(Request $request){
        $file = $request->file('avatar');
        $fileName = '';
        
        if(empty($file)){
            return $fileName;
        }
        else{
            $fileName = $file->getClientOriginalName();
            $file->move(public_path('admin/assets/images/users'), $file->getClientOriginalName());
        }

        return $fileName;
    }
}
