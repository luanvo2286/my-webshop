<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\conuntry;
use App\Http\Requests\CountryRequest;
use App\Http\Requests\EditCountryRequest;

class AdminCountryController extends Controller
{
    public function __construct()
    {
        // /$this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/country/add');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CountryRequest $request)
    {
        $data = $request->all();
        $country = new conuntry();
        $country->countryname = $data['country'];
        if($country->save()){
            return redirect()->back()->with('success','Create Country success');
        }else{
            return redirect()->back()->withErrors('Create Country ERRR.....');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function showCountry(){
        $data = conuntry::all();
        //dd($data);
        return view('admin/country/country', ['country'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = conuntry::findOrFail($id);
        return view('admin/country/edit', ['data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditCountryRequest $request, $id)
    {
        $data = $request->all();
        $country = conuntry::findOrFail($id);
        if($country->update($data)){
            return redirect()->back()->with('success',__('Update profile success'));
        }else{
            return redirect()->back()->withErrors('Update profile err');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteCountry = conuntry::find($id);
        if($deleteCountry->delete()){
            return redirect()->back()->with('success','Delete success!!');
        }else{
            return redirect()->back()->withErrors('Delete Failed!!');
        }
        
    }
}
