<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\BlogAddRequest;
use App\Http\Requests\BlogEditRequest;
use App\Models\blog;

class AdminBlogController extends Controller
{
    public function __construct()
    {
        //$this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = blog::all();
        $data = blog::paginate(3);
//dd($data);
        return view('admin/blog/blog', ['blogs'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/blog/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogAddRequest $request)
    {
        $data = $request->all();
        $tempFile = $this->UploadImg($request, 'image');
       
        $blog = new blog();
        $blog->title = $data['title'];
        $blog->description = $data['description'];
        $blog->image = $tempFile;
        $blog->content = $data['content'];
        
        if($blog->save()){
            return redirect()->back()->with('success','Create Blog success');
        }else{
            return redirect()->back()->withErrors('Create Country ERRR.....');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = blog::findOrFail($id);
        return view('admin/blog/edit', ['blog'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogEditRequest $request, $id)
    {   //cần check xem data cái nào rỗng, rỗng thì giữ nguyên giá trị cũ ở databse/ : description, image, title, content
        $blogID = blog::findOrFail($id);
        
        $data = request()->all();
        $data['image'] = $this->UploadImg($request, 'image');
       
        if(empty($data['content'])){
            $data['content'] = $blogID->content;
        }
        if(empty($data['image'])){
            $data['image'] = $blogID->image;
        }
        if(empty($data['description'])){
            $data['description'] = $blogID->description;
        }

        $blogID->title = $data['title'];
        $blogID->image = $data['image'];
        $blogID->content = $data['content'];
        $blogID->description = $data['description'];
        
        if($blogID->update()){ 
            return redirect()->back()->with('success', 'Edit this blog success!!');
        }else{
            return redirect()->back()->withErrors('Edit blog FAIL.....');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteBlog = blog::findOrFail($id);
        if($deleteBlog->delete()){
            return redirect()->back()->with('success','Delete success!!');
        }else{
            return redirect()->back()->withErrors('Delete Failed!!');
        }
    }

    public function UploadImg(Request $request, string $nameInput){
        $file = $request->file($nameInput);
        $fileName = '';
        
        if(empty($file)){
            return $fileName;
        }
        else{
            $fileName = $file->getClientOriginalName();
            $file->move(public_path('admin/assets/images/'), $file->getClientOriginalName());
        }

        return $fileName;
    }
}
