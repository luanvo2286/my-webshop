<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class registerMemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email'=>'required|unique:users,email|email',
            'password'=>'required|max:20|min:6',
            'avatar'=>'nullable|image|mimes:jpeg,png,jpg,gif',
            'phone'=>'nullable|min:10|max:10',
            'id_country'=>'nullable'
        ];
    }
}
