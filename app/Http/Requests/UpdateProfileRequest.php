<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'=>'required|max:20',
            'password'=>'nullable|min:6|max:20',
            'avatar'=>'image|mimes:jpeg,png,jpg,gif',
            'phone'=>'nullable|min:10|max:10'
        ];
    }

    public function messages(){
        return [
            'required'=>':attribute khong duoc de trong!',
            'max'=>':attribute khong duoc qua :max!',
            'min'=>':attribute khong duoc nho hon :min ky tu!',
            'image'=>':attribute phai la hinh anh',
            'mimes'=>':attribute hinh anh khong hop le'
        ];
    }

    public function attributes(){
        return [
            'name'=> 'Tên',
            'avatar'=>'Avatar'
        ];
    }
}
