
@extends('frontend.layouts.app')

@section('title')
    Blog | E-Shopper
@endsection

@section('content')
<div class="row">
    <div class="col-sm-4 col-sm-offset-1">
        <div class="login-form"><!--login form-->
            <h2>Login to your account</h2>
            <form action="" method="POST">
                @csrf
                <input name="email" type="email" placeholder="Email Address" />
                <input name="password" type="password" placeholder="Password" />
                <span style="display:block">
                    <input type="checkbox" name="remember_me" class="checkbox"> 
                    Keep me signed in
                </span>
                <div style="position: relative; height:50px;width:270px">
                    <button style="display: inline-block;position: absolute;bottom: 0;left: 10px;" name="submit" type="submit" class="btn btn-default">Login</button>
                    <span style="position: absolute;bottom: 0; left:50%;">Or</span>
                    <span style="position: absolute;bottom: 0;right: 10px;"><a href="{{route('member.register')}}">Register</a></span>
                </div>
                
            </form>
            <div>
                @if(session('success'))
                <div style="color:red; background-color: whitesmoke;">
                    {{session('success')}}
                </div>
                @endif
                @if($errors->any())
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div><!--/login form-->
    </div>
    
</div>
@endsection