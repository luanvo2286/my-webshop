@extends('frontend.layouts.app')

@section('title')
    Blog | E-Shopper
@endsection

@section('content')
<div class="col-sm-4">
    <div class="signup-form"><!--sign up form-->
        <h2>New User Signup!</h2>
        <form action="{{route('member.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input name="name" type="text" placeholder="Name"/>
            <input name="email" type="email" placeholder="Email Address"/>
            <input name="password" type="password" placeholder="Password"/>
            <input name="phone" type="text" placeholder="Phone"/>
            <select name="id_country" type="text" placeholder="Country">
                <option value="" >--select country--</option>
                <option value="1" >london</option>
                <option value="2">russia</option>
                <option value="3">vietnam</option>
            </select>
            <input name="avatar" type="file" placeholder="Avatar"/>
            {{-- <textarea name="message" type="text" placeholder="Message"></textarea> --}}
            <button name="submit" type="submit" class="btn btn-default">Signup</button>
        </form>
    </div><!--/sign up form-->
    <div>
        @if(session('success'))
        <div style="color:red; background-color: whitesmoke;">
            {{session('success')}}
        </div>
        @endif
        @if($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>

@endsection