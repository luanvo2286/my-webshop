@extends('frontend.layouts.app')

@section('title')
Blog Single | E-Shopper
@endsection



@section('content')
<div class="card-body">
    <form class="form-horizontal form-material" method="POST" action="{{route('member.update')}}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label class="col-md-12">Full Name</label>
            <div class="col-md-12">
                <input type="text" name="name" value="{{ Auth::user()->name; }}" class="form-control form-control-line">
            </div>
        </div>
        <div class="form-group">
            <label for="example-email" class="col-md-12">Email</label>
            <div class="col-md-12">
                <input disabled type="email" placeholder="{{ Auth::user()->email; }}" class="form-control form-control-line" name="example-email" id="example-email">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-12">Password</label>
            <div class="col-md-12">
                <input type="password" name="password" placeholder="input to change pass..." value="" class="form-control form-control-line">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-12">Address</label>
            <div class="col-md-12">
                <input type="text" name="phone" value="{{ Auth::user()->phone; }}" class="form-control form-control-line">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-12">Select Country</label>
            <div class="col-sm-12">
                <select name="id_country" class="form-control form-control-line">
                    @foreach ($country as $item)
                        <option value="{{$item['id']}}" {{ (($item['id'] == Auth::user()->id_country) ? 'selected' : '') }} >{{ $item['countryname'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-12">Phone No</label>
            <div class="col-md-12">
                <input type="text" name="phone" value="{{ Auth::user()->phone; }}" class="form-control form-control-line">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-12">Avatar</label>
            <div class="col-md-12">
                <input type="file" name="avatar" id="avatarToUpload" class="form-control form-control-line">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <button type="submit" name="submit" style="background-color:#FE980F;border:none;" class="btn btn-success">Update Profile</button>
            </div>
        </div>
        @if(session('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                {{session('success')}}
            </div>
        @endif
        @if($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </form>
</div>

@endsection