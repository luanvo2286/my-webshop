@extends('frontend.layouts.appAccount')

@section('title')
Blog Single | E-Shopper
@endsection

@section('script-account')
    
@endsection

@section('menu-left')
    <div class="col-sm-3">
        <div class="left-sidebar">
            <h2>Category</h2>
            <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="{{route('member.account')}}">
                                <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                ACCOUNT
                            </a>
                        </h4>
                    </div>
                    {{-- <div id="account" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                                <li>Account</li>
                                <li><a href="">Logout </a></li>
                                <li><a href="">Delete </a></li>
                            </ul>
                        </div>
                    </div> --}}
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="{{route('member.products.detail')}}">
                                <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                PRODUCT
                            </a>
                        </h4>
                    </div>
                    {{-- <div id="product" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                                <li>Product</li>
                                <li><a href="#">Delete</a></li>
                            </ul>
                        </div>
                    </div> --}}
                </div>
            </div><!--/category-products-->
        </div>
    </div>

@endsection

@section('content')
<div class="card-body">
    <h2>Your Products List</h2>
    <table id="table-product-list">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Image</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if ($dataProducts)
            @foreach ($dataProducts as $item)
            @php
                $arr = json_decode($item['image'], true);
                $image = $arr['0'];
                
            @endphp
            <tr>
                <td>{{$item['id']}}</td>
                <td>{{$item['name']}}</td>
                <td><img style="width: 50px;height:50px;" src="{{asset('uploads/frontend/product/'.$image)}}" alt=""></td>
                <td><span>${{$item['price']}}</span></td>
                <td><span><a href="{{route('member.products.edit', ['id'=>$item['id']])}}">Edit</a></span><span >Delete</span></td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="5" style="text-align: center;">Không có Product nào...!</td>
            </tr>
            @endif
            
        </tbody>
        <tfoot>
            <tr>
                <td colspan="5"><span><a style="color: white;" href="{{route('member.product.addnew')}}">Add New</a></span> </td>
            </tr>
        </tfoot>
    </table>
    
</div>

@endsection