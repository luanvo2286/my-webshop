@extends('frontend.layouts.appAccount')

@section('title')
Blog Single | E-Shopper
@endsection

@section('script-account')
    <script>
        $(document).ready(function(){
            $('#persent').css('display', 'none');
            $('#wrap-persent span').css('display', 'none');
            $('.dis-clas').prop('hidden', true);

            if($('#sale').val() == 1){
                $('#persent').css('display', 'inline-block');
                $('#wrap-persent span').css('display', 'inline-block');
            }

            $('#sale').click(function(){
                if($('#sale').val() == 0){
                    $('#persent').css('display', 'none');
                    $('#wrap-persent span').css('display', 'none');

                }else{
                    $('#persent').css('display', 'inline-block');
                    $('#wrap-persent span').css('display', 'inline-block');
                }
            })

            $('#category').click(function(){
                if($('#category').val() == ''){
                    $('.dis-clas').prop('hidden', true);
                }
                if($('#category').val() == 1){
                    $('.dis-clas').attr('hidden', true);
                    $('.1').removeAttr('hidden');
                }else if($('#category').val() == 2){
                    $('.dis-clas').attr('hidden', true);
                    $('.2').removeAttr('hidden');
                }
                else if($('#category').val() == 3){
                    $('.dis-clas').attr('hidden', true);
                    $('.3').removeAttr('hidden');
                }
                else if($('#category').val() == 4){
                    $('.dis-clas').attr('hidden', true);
                    $('.4').removeAttr('hidden');
                }
            })
            
        })
    </script>
@endsection

@section('menu-left')
    <div class="col-sm-3">
        <div class="left-sidebar">
            <h2>Category</h2>
            <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="{{route('member.account')}}">
                                <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                ACCOUNT
                            </a>
                        </h4>
                    </div>
                    {{-- <div id="account" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                                <li>Account</li>
                                <li><a href="">Logout </a></li>
                                <li><a href="">Delete </a></li>
                            </ul>
                        </div>
                    </div> --}}
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="{{route('member.products.detail')}}">
                                <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                PRODUCT
                            </a>
                        </h4>
                    </div>
                    {{-- <div id="product" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                                <li>Product</li>
                                <li><a href="#">Delete</a></li>
                            </ul>
                        </div>
                    </div> --}}
                </div>
            </div><!--/category-products-->
        </div>
    </div>

@endsection

@section('content')
    <div class="card-body">
        <h2>Edit product</h2>
        <form method="post" enctype="multipart/form-data">
            @csrf
            <input type="text" name="name" value="{{$dataProduct['name']}}" placeholder="Name">
            <input type="text" name="price" value="{{$dataProduct['price']}}" placeholder="Price">
            <select name="category" id="category">
                <option selected  value="">Chọn thể loại</option>
                @foreach ($categories as $item)
                <option  value="{{$item['id']}}" {{($item['id'] == $dataProduct['id_category']) ? 'selected' : ''}}>{{$item['category']}}</option>
                @endforeach
                
            </select>
            <select name="brand" id="brand">
                <option value="">chon hang</option>
                @foreach ($brands as $item)
                @if ($item['id_category'] == 1)
                <option class="dis-clas {{$item['id_category']}}" {{($item['id'] == $dataProduct['id_brand']) ? 'selected' : ''}} value="{{$item['id']}}">{{$item['brand']}}</option>
                @endif
                @if ($item['id_category'] == 2)
                <option class="dis-clas {{$item['id_category']}}" {{($item['id'] == $dataProduct['id_brand']) ? 'selected' : ''}} value="{{$item['id']}}">{{$item['brand']}}</option>
                @endif
                @if ($item['id_category'] == 3)
                <option class="dis-clas {{$item['id_category']}}" {{($item['id'] == $dataProduct['id_brand']) ? 'selected' : ''}} value="{{$item['id']}}">{{$item['brand']}}</option>
                @endif
                @if ($item['id_category'] == 4)
                <option class="dis-clas {{$item['id_category']}}" {{($item['id'] == $dataProduct['id_brand']) ? 'selected' : ''}} value="{{$item['id']}}">{{$item['brand']}}</option>
                @endif
                @endforeach
            </select>
            <select id="sale" name="sale">
                <option value="0" {{($dataProduct['status'] == 0) ? 'selected' : ''}}>New</option>
                <option value="1" {{($dataProduct['status'] == 1) ? 'selected' : ''}}>Sale</option>
            </select>
            
            <div id="wrap-persent">
                <input type="text" name="persent" id="persent" value="{{$dataProduct['sale']}}" placeholder="0">
                <span>%</span>
            </div>
            
            <input style="display: block" type="text" name="company" value="{{$dataProduct['company']}}" id="company" placeholder="Company profile">

            
            <input type="file" multiple name="image[]" id="image">
            
            @foreach (json_decode($dataProduct['image'], true) as $item)
            <div class="img-checkbox">
                <ul>
                    <li>
                      <input type="checkbox" id="{{$item}}" name="check[]" value="{{$item}}"/>
                      <label for="{{$item}}"><img style="width: 50px;height:50px;" src="{{asset('uploads/frontend/product/'.$item)}}" /></label>
                    </li>
                  </ul>
            </div>
                {{-- <img style="width:50px; height:50px;display:block;" src="{{asset('uploads/frontend/product/'.$item)}}" alt="">
                <input type="checkbox" id="{{$item}}" value="{{$item}}"><br> --}}
            @endforeach         
            
            <textarea name="detail" id="detail" cols="30" rows="10" placeholder="Detail">{{$dataProduct['detail']}}</textarea>

            <button type="submit" name="submit">Signup</button>
            
        </form>
        <div>
            @if(session('success'))
            <div style="color:red; background-color: whitesmoke;">
                {{session('success')}}
            </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
@endsection