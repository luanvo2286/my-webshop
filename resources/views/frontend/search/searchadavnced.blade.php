@extends('frontend.layouts.app')

@section('title')
    Search | E-Shopper
@endsection
@section('script')
<script>
    $(document).ready(function(){
            if($('#category').val() == ''){
                $('.dis-clas').prop('hidden', true);
            }
            if($('#category').val() == 1){
                $('.dis-clas').attr('hidden', true);
                $('.1').removeAttr('hidden');
            }else if($('#category').val() == 2){
                $('.dis-clas').attr('hidden', true);
                $('.2').removeAttr('hidden');
            }
            else if($('#category').val() == 3){
                $('.dis-clas').attr('hidden', true);
                $('.3').removeAttr('hidden');
            }
            else if($('#category').val() == 4){
                $('.dis-clas').attr('hidden', true);
                $('.4').removeAttr('hidden');
            }
        $('#category').click(function(){
            if($('#category').val() == ''){
                $('.dis-clas').prop('hidden', true);
            }
            if($('#category').val() == 1){
                $('.dis-clas').attr('hidden', true);
                $('.1').removeAttr('hidden');
            }else if($('#category').val() == 2){
                $('.dis-clas').attr('hidden', true);
                $('.2').removeAttr('hidden');
            }
            else if($('#category').val() == 3){
                $('.dis-clas').attr('hidden', true);
                $('.3').removeAttr('hidden');
            }
            else if($('#category').val() == 4){
                $('.dis-clas').attr('hidden', true);
                $('.4').removeAttr('hidden');
            }
        })
    })
</script>
@endsection
@section('content')
<div class="features_items search"><!--features_items-->
    <h2 class="title text-center">Features Items</h2>
    
    <div style="width:100%;height:auto;">
        <form method="post" name="searchadvanced">
           @csrf
            <div class="class-search">
                <input type="text" name="key" value="{{isset($currentData['key'])?$currentData['key']:''}}" placeholder="Name">
            </div>
            <div class="class-search">
                <select name="price" id="price">
                    <option value="">Choose Price</option>
                    <option {{ isset($currentData['price'])?(('0-100') === ($currentData['price'])) ? 'selected':'':'' }} value="0-100">0 - $100</option>
                    <option {{ isset($currentData['price'])?(('100-500') === ($currentData['price'])) ? 'selected':'':'' }} value="100-500">$100- $500</option>
                    <option {{ isset($currentData['price'])?(('500-1000') === ($currentData['price'])) ? 'selected':'':'' }} value="500-1000">$500 - $1000</option>
                </select>
            </div>
            <div class="class-search">
                <select name="category" id="category">
                    <option value="">Category</option>
                    @foreach($categories as $item)
                    <option {{ isset($currentData['category'])?($item['id'])==($currentData['category']) ? 'selected' : '':'' }} value="{{$item['id']}}">{{$item['category']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="class-search">
                <select name="brand" id="brand">
                    <option value="">Brand</option>
                    @foreach($brands as $item)
                        @for ($i = 0; $i < 4; $i++)
                            @if ($item['id_category'] == $i)
                            <option class="dis-clas {{$item['id_category']}}" {{(isset($currentData['brand']) ? ($item['id'] == $currentData['brand'] ? 'selected' : '') : '' )}} value="{{$item['id']}}">{{$item['brand']}}</option>
                            @endif
                        @endfor
                    @endforeach
                </select>
            </div>
            <div class="class-search">
                <select name="status" id="status">
                    <option value="">Status</option>
                    <option {{(isset($currentData['status']) ? ($currentData['status'] == 0 ? 'selected' : '') : '')}} value="0">New</option>
                    <option {{(isset($currentData['status']) ? ($currentData['status'] == 1 ? 'selected' : '') : '')}} value="1">Sale</option>
                </select>
            </div>
            <div class="class-search">
                <button class="btn" type="submit" name="submit">Find</button>
            </div>
        </form>
    </div>

    @if (isset($listProduct) && count($listProduct) > 0)
    @foreach ($listProduct as $item)
    @php
        $image = json_decode($item->image, true);
    @endphp
        <div class="col-sm-4">
            <div class="product-image-wrapper" id="{{$item->id}}">
                
                    <div class="single-products" >
                        <div class="productinfo text-center">
                            <div style="width: 256px; height:256px;"><img style="width: 100%; height:auto;" src="{{asset('uploads/frontend/product/'.$image[0])}}" alt="" /></div>
                            <h2>${{$item->price}}</h2>
                            <p>{{$item->name}}</p>
                            <a href="" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                        </div>
                        
                        <div class="product-overlay">
                            <div class="overlay-content">
                                <h2>${{$item->price}}</h2>
                                <p>{{$item->name}}</p>
                                <a class="btn btn-default add-to-cart cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                            </div>
                        </div>
                        
                    </div>
                    <div class="choose">
                        <ul class="nav nav-pills nav-justified">
                            <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                            <li><a href="{{route('product.detail', ['id'=>$item->id])}}"><i class="fa fa-plus-square"></i>Watch Product</a></li>
                        </ul>
                    </div>
                
            </div>
        </div>
    @endforeach
    @else
        <div class="text-center col-sm-12 m-b-15">
            Khong co product nao.....
        </div>
    @endif
    <div class="col-sm-12 text-center">
        @if (isset($listProduct) && count($listProduct) > 0)
        {{$listProduct->appends(Request::all())->links('pagination::bootstrap-4');}} 
        {{-- ->appends(Request::all()) : cái này truyền lại các param bằng request qua trang kế tiếp. --}}
        @endif
    </div>
</div><!--features_items-->

@endsection