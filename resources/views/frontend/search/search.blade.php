@extends('frontend.layouts.app')

@section('title')
    Search | E-Shopper
@endsection
@section('script')
<script>
    $(document).ready(function(){
        
    })
</script>
@endsection
@section('content')
<div class="features_items search"><!--features_items-->
    <h2 class="title text-center">Features Items</h2>
    @if (isset($listProduct) && count($listProduct) > 0)
    @foreach ($listProduct as $item)
    @php
        $image = json_decode($item->image, true);
    @endphp
        <div class="col-sm-4">
            <div class="product-image-wrapper" id="{{$item->id}}">
                
                    <div class="single-products" >
                        <div class="productinfo text-center">
                            <div style="width: 256px; height:256px;"><img style="width: 100%; height:auto;" src="{{asset('uploads/frontend/product/'.$image[0])}}" alt="" /></div>
                            <h2>${{$item->price}}</h2>
                            <p>{{$item->name}}</p>
                            <a href="" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                        </div>
                        
                        <div class="product-overlay">
                            <div class="overlay-content">
                                <h2>${{$item->price}}</h2>
                                <p>{{$item->name}}</p>
                                <a class="btn btn-default add-to-cart cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                            </div>
                        </div>
                        
                    </div>
                    <div class="choose">
                        <ul class="nav nav-pills nav-justified">
                            <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                            <li><a href="{{route('product.detail', ['id'=>$item->id])}}"><i class="fa fa-plus-square"></i>Watch Product</a></li>
                        </ul>
                    </div>
                
            </div>
        </div>
    @endforeach
    @endif
<div class="col-sm-12 text-center ">
    @if (isset($listProduct) && count($listProduct) > 0)
    {{$listProduct->appends(request()->all())->links('pagination::bootstrap-4');}}
    @endif
</div>
    
</div><!--features_items-->

@endsection