@extends('frontend.layouts.app')

@section('title')
    Blog | E-Shopper
@endsection

@section('content')

    <div class="blog-post-area">
        <h2 class="title text-center">Latest From our Blog</h2>

        @foreach ($dataBlogs as $key=>$item)
        
        <div class="single-blog-post">
            {{-- --title------------------? --}}
            <h3>{{$item->title}}</h3>
            <div class="post-meta">
                <ul>
                    <li><i class="fa fa-user"></i> Mac Doe</li>
                    <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                    <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                </ul>
                <span>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                </span>
            </div>
            <a href="">
                {{-- --img------------------ --}}
                <img src="{{asset('admin/assets/images/'.$item->image)}}" alt="">
            </a>
            {{-- ----------------description----------------- --}}
            <p>{{ $item->description }}</p>
            <a  class="btn btn-primary" href="{{ route('blog.single',['id'=>$item->id]) }}">Read More</a>
            
        </div>
            {{-- @if (++$key == 3)
                @php
                    break;
                @endphp
            @endif --}}
        @endforeach
        
        {{-- <div class="pagination-area">
            <ul class="pagination">
                <li><a href="" class="active">1</a></li>
                <li><a href="">2</a></li>
                <li><a href="">3</a></li>
                <li><a href=""><i class="fa fa-angle-double-right"></i></a></li>
            </ul>
        </div> --}}
        <div class="float-left" style="margin: 5px">{{ $dataBlogs->links('pagination::bootstrap-4') }}</div>
        
    </div>
    

@endsection