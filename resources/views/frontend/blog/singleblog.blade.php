@extends('frontend.layouts.app')

@section('title')
Blog Single | E-Shopper
@endsection

@section('script-rating')
    <script> 
        $(document).ready(function(){
            
            //vote
            $('.ratings_stars').hover(
                // Handles the mouseover
                function() {
                    $(this).prevAll().andSelf().addClass('ratings_hover');
                    // $(this).nextAll().removeClass('ratings_vote'); 
                },
                function() {
                    $(this).prevAll().andSelf().removeClass('ratings_hover');
                    // set_votes($(this).parent());
                }
            );

            $('.ratings_stars').click(function(){
        
                
                if("{{Auth::check()}}"){

                    var Values =  $(this).find("input").val();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                       type: "POST",
                       url: "{{ url('/blog/single/rating') }}",
                       data: {
                           user_id: {{Auth::check() ? Auth::id() : 0 }},
                           product_id: {{ $product_id }},
                           rate_value: Values   
                       },
                       success: function(response) {
                            
                            if(response.info)
                            {
                                alert(response.info);
                            }else{
                                $('.rate-np').text(response.data)
                                if ($(this).hasClass('ratings_over')) {
                                    $('.ratings_stars').removeClass('ratings_over');
                                    $(this).prevAll().andSelf().addClass('ratings_over');
                                } else {
                                    $(this).prevAll().andSelf().addClass('ratings_over');
                                }
                            }
                         }
                   })
                  
                }else{
                    alert('Login first!!');
                }
                    
            });

            $('.reply-btn').click(function(){

                //check login chua.
                if("{{Auth::check()}}"){
                    var name = $(this).closest('.media-body').find('.sinlge-post-meta li:nth-child(1)').text()
                    
                    var level = $(this).closest('.media-body').find('.sinlge-post-meta').attr('id')
                    $('.text-area').find('textarea').text("Reply to " + name + " :" )
                    $('#level').val(level)

                    $('html, body').animate({
                        scrollTop:$('.replay-box').offset().top
                    }, 'slow');
                }else{
                    alert('Login first!!')
                }
                
            });

            $('.text-area').find('textarea').click(function(){
                //check login chua.
                if(!"{{Auth::check()}}"){
                    alert('Login first!!')
                }
            })

            $('#form-cmnt').submit(function(){
                if("{{Auth::check()}}"){
                    return true;
                }else{
                    alert('Login first!!')
                    return false;
                }
                
            })
        });
    </script>
@endsection

@section('content')
    <div class="blog-post-area">
        <h2 class="title text-center">Latest From our Blog</h2>
        <div class="single-blog-post">
            {{-- set title dựa vào id --}}
            <h3>{{ $contentData->title }}</h3> 
            <div class="post-meta">
                <ul>
                    <li><i class="fa fa-user"></i> Mac Doe</li>
                    <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                    <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                </ul>
                <span>
                    {{-- dựa vào dữ liêu rating ở database ta set bao nhiêu sao ở đây --}}
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-half-o"></i>
                </span>
            </div>

            {!! $contentData->content !!}

            <div class="pager-area">
                {{-- xem bài viết tiếp theo nếu còn ,ko còn thì next ko highlight --}}
                <ul class="pager pull-right">
                    <li><a href="#">Pre</a></li>
                    <li><a href="#">Next</a></li>
                </ul>
            </div>
        </div>
    </div><!--/blog-post-area-->

    {{-- <div class="rating-area">
        <ul class="ratings">
            <li class="rate-this">Rate this item:</li>
            <li>
                <i class="fa fa-star color"></i>
                <i class="fa fa-star color"></i>
                <i class="fa fa-star color"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </li>
            <li class="color">(6 votes)</li>
        </ul>
        <ul class="tag">
            <li>TAG:</li>
            <li><a class="color" href="">Pink <span>/</span></a></li>
            <li><a class="color" href="">T-Shirt <span>/</span></a></li>
            <li><a class="color" href="">Girls</a></li>
        </ul>
    </div><!--/rating-area--> --}}
    <div class="rate">
        <div class="vote">
            {{-- <form action="" method="POST">
                @csrf --}}
                @for ($i = 1; $i < 6; $i++)
                    <div class="star_1 ratings_stars {{ (round($rateData)) >= $i ? 'ratings_over' : '' }}"><input name="product-rating" value="{{$i}}" type="hidden"></div>
                @endfor
                
                <span class="rate-np">
                    
                        {{$rateData}}
                    
                </span>
                {{-- <div ><button type="submit" name="rate-button">Rate This Product</button></div>
            </form> --}}
        </div> 
    </div>

    <div class="socials-share">
        <a href=""><img src="images/blog/socials.png" alt=""></a>
    </div><!--/socials-share-->

    <div class="media commnets">
        <h2>Media Commnents</h2>
        <br>
        <a class="pull-left" href="#">
            <img class="media-object" src="{{asset('Frontend/images/blog/man-one.jpg')}}" alt="">
        </a>
        <div class="media-body">
            <h4 class="media-heading">Annie Davis</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            <div class="blog-socials">
                <ul>
                    <li><a href=""><i class="fa fa-facebook"></i></a></li>
                    <li><a href=""><i class="fa fa-twitter"></i></a></li>
                    <li><a href=""><i class="fa fa-dribbble"></i></a></li>
                    <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                </ul>
                <a class="btn btn-primary" href="">Other Posts</a>
            </div>
        </div>
    </div><!--Comments-->
    <div class="response-area">
        <h2>3 RESPONSES</h2>
        <ul class="media-list">
            @foreach ($cmts as $item)
                @if ($item['level'] == 0)
                    <li class="media">
                        <a class="pull-left" href="#">
                            {{-- hình ảnh ở dưới lấy từ bảng users thông qua user_id của bảng Comments --}}
                            <img style="width:100px;height:100px;" class="media-object" src="{{asset('frontend/images/users/'.$item['image'])}}" alt=""> 
                        </a>
                        <div class="media-body">
                            <ul class="sinlge-post-meta" id="{{$item['id']}}">
                                <li><i class="fa fa-user"></i>{{$item['name']}}</li>
                                <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                            </ul>
                            <p>{{$item['cmt']}}</p>
                            <a class="btn btn-primary reply-btn"><i class="fa fa-reply"></i> Reply</a>
                        </div>
                    </li>
                    @foreach ($cmts as $childItem)
                        @if ($childItem['level'] == $item['id'])
                            <li class="media second-media">
                                <a class="pull-left" href="#">
                                    <img style="width:100px;height:100px;" class="media-object" src="{{asset('frontend/images/users/'.$childItem['image'])}}" alt="">
                                </a>
                                <div class="media-body">
                                    <ul class="sinlge-post-meta" id="{{$childItem['level']}}">
                                        <li><i class="fa fa-user"></i>{{$childItem['name']}}</li>
                                        <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                        <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                                    </ul>
                                    <p>{{$childItem['cmt']}}</p>
                                    <a class="btn btn-primary reply-btn"><i class="fa fa-reply"></i>Reply</a>
                                </div>
                            </li>
                        @endif
                    @endforeach
                @endif
            @endforeach
            
        </ul>					
    </div><!--/Response-area-->
    <div class="replay-box">
        <div class="row">
            
            <div class="col-sm-8">
                <div class="text-area">
                    <div class="blank-arrow">
                        <label>Your Comment</label>
                    </div>
                    <span>*</span>
                    {{-- action="{{route('blog.comments',['id'=>$product_id])}}" --}}
                    <form method="POST" id="form-cmnt" action="{{route('blog.comments',['id'=>$product_id])}}"> 
                        @csrf
                        <input id="level" name="level" type="hidden" value="0">
                        <textarea name="message" rows="11"></textarea>
                        <button type="submit" name="submit" class="btn btn-primary">post comment</button>
                    </form>
                    
                </div>
            </div>
        </div>
    </div><!--/Repaly Box-->
    
@endsection