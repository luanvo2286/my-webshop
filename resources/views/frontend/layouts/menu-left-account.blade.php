<div class="col-sm-3">
    <div class="left-sidebar">
        <h2>Category</h2>
        <div class="panel-group category-products" id="accordian"><!--category-productsr-->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a href="{{route('member.account')}}">
                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                            ACCOUNT
                        </a>
                        
                    </h4>
                </div>
                {{-- <div id="account" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul>
                            <li>Account</li>
                            <li><a href="">Logout </a></li>
                            <li><a href="">Delete </a></li>
                        </ul>
                    </div>
                </div> --}}
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a href="{{route('member.products.detail')}}">
                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                            PRODUCT
                        </a>
                    </h4>
                </div>
                {{-- <div id="product" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul>
                            <li>Product</li>
                            <li><a href="#">Delete</a></li>
                        </ul>
                    </div>
                </div> --}}
            </div>
        </div><!--/category-products-->
    </div>
</div>