<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link href="{{asset('frontend/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/animate.css')}}" rel="stylesheet">
	<link href="{{asset('frontend/css/main.css')}}" rel="stylesheet">
	<link href="{{asset('frontend/css/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/product.css')}}" rel="stylesheet">

    <link type="text/css" rel="stylesheet" href="{{asset('frontend/css/rate.css')}}"> 
    <script src="{{asset('frontend/js/jquery-1.9.1.min.js')}}"></script>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <script>
        if(screen.width <= 736){
            document.getElementById("viewport").setAttribute("content", "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no");
        }
    </script>
    <script>
        $(document).ready(function(){
            
            $('#search').keypress(function(event){
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    var keyvalue = $(this).val();
                    if(keycode == '13'){
                        //$(this).find('.form-search').attr('action', '{{route('search.name',['key'])}}'+keyvalue);
                        document.form-search.submit();
                    }
            });
            
            $('.well').mouseup(function(){
                var valueRange = $('.tooltip-inner').text().split(":")
                //var arr = valueRange.split(":");
                var priceRange = {start: valueRange[0].trim(), end: valueRange[1].trim()}
                var jsonData = {price: priceRange}
                                
                $.ajax({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                 },
                contentType: 'application/json',
                type: "POST", 
                dataType: 'json',
                url: "{{ url('/search/price-range') }}",
                data: JSON.stringify(jsonData),
                success: function(res){
                    //console.log(res.listproduct);
                    var htmls = "";
                    //htmls.push(`<h2 class="title text-center">Features Items</h2>`)
                    htmls+="<h2 class='title text-center'>Features Items</h2>"
                    Object.keys(res.listproduct).map((key)=>{
                        var name = res.listproduct[key].name
                        var img = JSON.parse(res.listproduct[key].image)[0]
                        var id = res.listproduct[key].id
                        var price = res.listproduct[key].price
                        
                        //console.log(img)
                        // htmls.push(
                        //     `
                        //     <div class="col-sm-4">
                        //         <div class="product-image-wrapper" id="${id}">
                                    
                        //             <div class="single-products" >
                        //                 <div class="productinfo text-center">
                        //                     <div style="width: 256px; height:256px;"><img style="width: 100%; height:auto;" src="uploads/frontend/product/${img}" alt="" /></div>
                        //                         <h2>${price}</h2>
                        //                         <p>${name}</p>
                        //                         <a href="" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                        //                     </div>
                                            
                        //                     <div class="product-overlay">
                        //                         <div class="overlay-content">
                        //                             <h2>${price}</h2>
                        //                             <p>${name}</p>
                        //                             <a class="btn btn-default add-to-cart cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                        //                         </div>
                        //                     </div>
                                            
                        //                 </div>
                        //                 <div class="choose">
                        //                     <ul class="nav nav-pills nav-justified">
                        //                         <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                        //                         <li><a href=""><i class="fa fa-plus-square"></i>Watch Product</a></li>
                        //                     </ul>
                        //                 </div>
                                    
                        //         </div>
                        //     </div>
                        //     `
                        //)
                        var xx = '{{route("product.detail", ["id"=>'+id+'])}}'
                        htmls +=
                            
                            '<div class="col-sm-4">' + 
                                '<div class="product-image-wrapper" id="' + id + '">'+
                                    '<div class="single-products" >'+
                                        '<div class="productinfo text-center">'+
                                            '<div style="width: 256px; height:256px;"><img style="width: 100%; height:auto;" src="uploads/frontend/product/'+img+'" alt="" /></div>'+
                                                '<h2>'+price+'</h2>'+
                                                '<p>'+name+'</p>'+
                                                '<a href="" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>'+
                                            '</div>'+
                                            '<div class="product-overlay">'+
                                                '<div class="overlay-content">'+
                                                    '<h2>'+price+'</h2>'+
                                                    '<p>'+name+'</p>'+
                                                    '<a class="btn btn-default add-to-cart cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="choose">'+
                                            '<ul class="nav nav-pills nav-justified">'+
                                                '<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>'+
                                                '<li><a href="'+xx+'"><i class="fa fa-plus-square"></i>Watch Product</a></li>'+
                                            '</ul>'+
                                        '</div>'+
                                '</div>'+
                            '</div>'

                        
                    });

                    $('.features_items').html(htmls)
                    
                }
            })
        })
    })
    </script>

{{-- script vote for product --}}
    @yield('script')
    

</head><!--/head-->
<body>
    @include('frontend.layouts.header')

    @include('frontend.layouts.slide')
    @if (!str_contains(Request::route()->getName(), 'nonleft'))
        <section>
                <div class="container">
                    <div class="row">
                        
                        @if ((Request::route()->getName() === 'member.account') || (Request::route()->getName() === 'member.products.detail' ))
                            @include('frontend.layouts.menu-left-account')
                        @else
                            @include('frontend.layouts.menu-left')
                        @endif
                        
                        <div class="col-sm-9 padding-right" >
                            @yield('content')
                        </div>
                    </div>
                </div>
        </section>  
    @endif 
    <section id="cart_items">
        @yield('cart-items')
        
    </section> <!--/#cart_items-->

    <section id="do_action">
        @yield('do-action')
        
    </section><!--/#do_action-->

    
    @include('frontend.layouts.footer')
    
    <script src="{{asset('frontend/js/jquery.js')}}"></script>
	<script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('frontend/js/jquery.scrollUp.min.js')}}"></script>
	<script src="{{asset('frontend/js/price-range.js')}}"></script>
    <script src="{{asset('frontend/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('frontend/js/main.js')}}"></script>
    
    {{-- <script src="{{asset('frontend/js/jquery-1.9.1.min.js')}}"></script> --}}
</body>