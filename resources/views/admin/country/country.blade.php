@extends('admin.layouts.app')

@section('title')
    Nice admin Template - The Ultimate Multipurpose admin template
@endsection

@section('content')
<div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Table Header</h4>
            <h6 class="card-subtitle">Similar to tables, use the modifier classes .thead-light to make <code>&lt;thead&gt;</code>s appear light.</h6>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Country Name</th>
                        <th scope="col">Postal code</th>
                        <th scope="col">Handle</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($country as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->countryname }}</td>
                            <td>-</td>
                            <td><a href="{{ route('edit.country', ['id'=>$item->id]) }}">Edit</a> --- <a href="{{ route('delete.country', ['id'=>$item->id]) }}">Delete</a></td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3"><a href="{{ route('create.country') }}">Add Country</a></td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div>
            @if(session('success'))
            <div style="color:red; background-color: whitesmoke;">
                {{session('success')}}
            </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
</div>
    {{-- <div>
        <table>
            <tr>
                <th>ID</th>
                <th>Country Name</th>
                <th>ACTION</th>
            </tr>
            @foreach ($country as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->countryname }}</td>
                <td><a href="">Edit</a> --- <a href="">Delete</a></td>
            </tr>
            @endforeach
            <tr>
                <td colspan="2"><a href="">Add Country</a></td>
            </tr>
            
        </table>
    </div> --}}
@endsection