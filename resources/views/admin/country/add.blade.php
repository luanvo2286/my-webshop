@extends('admin.layouts.app')

@section('title')
    Nice admin Template - The Ultimate Multipurpose admin template
@endsection

@section('content')
    <div class="col-12">
        <form class="form-horizontal form-material" action="" method="POST">
            @csrf
            <div class="form-group">
                <label class="col-md-12" for="">Country</label>
                <div class="col-md-3">
                    <input type="text" name="country" id="country" class="form-control form-control-line">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" name="submit" class="btn btn-success">Create country</button>
                </div>
            </div>
            @if(session('success'))
            <div>
                {{session('success')}}
            </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </form>
    </div>
@endsection