@extends('admin.layouts.app')

@section('title')
    Nice admin Template - The Ultimate Multipurpose admin template
@endsection
@section('content')
<div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Blog Manager</h4>
            
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Title</th>
                        <th scope="col">Image</th>
                        <th scope="col">Description</th>
                        <th scope="col">Handle</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($blogs as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->title }}</td>
                            <td><img style="width: 100px; height:100px;" src="{{($item->image)?asset('admin/assets/images/'.$item->image):asset('admin/assets/images/blank-image.jpg') }}" alt=""></td>
                            <td>{{ $item->description }}</td>
                            <td><a href="{{route('edit.blog',['id'=>$item->id])}}">Edit</a> --- <a href="{{route('delete.blog', ['id'=>$item->id])}}">Delete</a></td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        
                        <td colspan="3"><a href="{{ route('create.blog') }}">Add blog</a></td>
                    </tr>
            

                </tfoot>
               
            </table>
        </div>
        <div>
            {{ $blogs->links('pagination::bootstrap-4') }}
        </div>
    </div>
@endsection