<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Spatie\FlareClient\Api;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\BlogController;
use App\Http\Controllers\Frontend\ajax\AjaxSingleBlog;
use App\Http\Controllers\Frontend\ajax\AjaxPriceRange;
use App\Http\Controllers\Frontend\MemberController;
use App\Http\Controllers\Frontend\ProductController;
use App\Http\Controllers\Admin\AdminHomeController;
use App\Http\Controllers\Admin\AdminUserController;
use App\Http\Controllers\Admin\AdminCountryController;
use App\Http\Controllers\Admin\AdminBlogController;
use App\Http\Controllers\Frontend\CartController;
use App\Http\Controllers\Frontend\SearchController;
use App\Http\Controllers\Auth\LoginController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// fronend

Route::get('/', [HomeController::class, 'index']);
Route::prefix('custom')->middleware('memberNotLogin')->group(function(){
    //login, logout, register
    Route::get('/login', [MemberController::class, 'login'])->name('memberlogin');

    Route::post('/login', [MemberController::class, 'loginMember']);
    Route::get('/register', [MemberController::class, 'register'])->name('member.register');
    Route::post('/register', [MemberController::class, 'storeMember'])->name('member.store');  
});

Route::prefix('member')->middleware('member')->group(function(){
    Route::get('/', [HomeController::class, 'index']);
    //blog
    Route::get('/blog', [BlogController::class, 'index'])->name('blog');
    Route::get('/blog/single/{id}', [BlogController::class, 'showBlogSingle'])->name('blog.single');
    Route::post('/blog/single/rating', [AjaxSingleBlog::class, 'rateProduct']);
    Route::post('/blog/single/comment/{id}', [BlogController::class, 'comments'])->name('blog.comments');

    //account
    Route::get('/account', [MemberController::class, 'showAccount'])->name('member.account');
    Route::post('/account/update', [MemberController::class, 'updateAccount'])->name('member.update');
    //product
    Route::get('/product', [ProductController::class, 'showProduct'])->name('member.products.detail');
    Route::get('/product/addnew',[ProductController::class, 'addProduct'])->name('member.product.addnew');
    Route::post('/product/addnew',[ProductController::class, 'addProductPost'])->name('member.product.addnew.post');

    Route::get('/product/update/{id}', [ProductController::class, 'editProduct'])->name('member.products.edit');
    Route::post('/product/update/{id}', [ProductController::class, 'updateProduct'])->name('member.products.update');

    //product detail
    Route::post('/product/detail/addtocart', [ProductController::class, 'AddToCart']);

    //cart
    Route::get('/cart', [CartController::class, 'index'])->name('nonleft.cart');
    Route::get('/checkout', [CartController::class, 'checkout'])->name('nonleft.checkout');

    //logoout
    Route::get('/logout', [MemberController::class, 'logout'])->name('memberlogout');
});
//home
Route::get('/home', [HomeController::class, 'index'])->name('home');

//product detail 
Route::get('/product/detail/{id}' , [ProductController::class, 'show'])->name('product.detail');

//contact-us
//Route::get('/contact', [ContactController::class, 'index'])->name('nonleft.contact.us');

//search
Route::get('/search-name',[SearchController::class, 'searchName'])->name('search.name');
// Route::get('/search-advanced', [SearchController::class, 'searchAdvanced'])->name('search.advanced');
Route::post('/search-advanced', [SearchController::class, 'searchPost']);
Route::get('/search-advanced/{param?}', [SearchController::class, 'searchAdvanced'])->name('search.advanced');
Route::post('/search/price-range', [AjaxPriceRange::class, 'priceRange']);

//khi submit form có @crfs thì form sẽ tự động được gửi lên route của chính form đó (nghĩa là route lúc này sẽ là đường link mà form đó chứa.) route này là (/member/register), vì mình chưa đặt tên cho nó,
//còn khi đặt tên rồi thì form sẽ có action"{{route('ten route',['key'=>value])}}" ví dụ:Route::post('/blog/single/comment/{id}', [App\Http\Controllers\Frontend\BlogController::class, 'comments'])->name('blog.comments');

// backend ,e login xong thi moi vao 
Auth::routes();

//login loogout này là code có sẳn của framework.


Route::group([
    'prefix' => 'admin',
], function () {
    
    Route::get('/login', [LoginController::class, 'showLoginForm'])->name('admin.login');
    Route::post('/login', [LoginController::class, 'login']);
    Route::get('/logout', [LoginController::class, 'logout']);
});

Route::group([
    'prefix'=>'admin',
    'middleware'=>'admin'
], function(){
    Route::get('/home', [AdminHomeController::class, 'index'])->name('admin.dashboard');
    Route::get('/profile', [AdminUserController::class, 'index'])->name('admin.profile');
    Route::post('/profile', [AdminUserController::class, 'update']);
    Route::get('/country', [AdminCountryController::class, 'showCountry'])->name('admin.country');
    Route::get('/create/country', [AdminCountryController::class, 'index'])->name('create.country');
    Route::post('/create/country', [AdminCountryController::class, 'store']);
    Route::get('/edit/country/{id}', [AdminCountryController::class, 'edit'])->name('edit.country');
    Route::post('/edit/country/{id}', [AdminCountryController::class, 'update']);
    Route::get('/delete/country/{id}', [AdminCountryController::class, 'destroy'])->name('delete.country');
    Route::get('/blog', [AdminBlogController::class, 'index'])->name('admin.blog');
    Route::get('/create/blog', [AdminBlogController::class, 'create'])->name('create.blog');
    Route::post('/create/blog', [AdminBlogController::class, 'store']);
    Route::get('/edit/blog/{id}', [AdminBlogController::class, 'edit'])->name('edit.blog');
    Route::post('/edit/blog/{id}', [AdminBlogController::class, 'update']);
    Route::get('/delete/blog/{id}', [AdminBlogController::class, 'destroy'])->name('delete.blog');
});



//api
